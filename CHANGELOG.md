# Changelog
## [1.0.11] - 2020-04-15
### Added
- When switch to rts camera, it will be raised to a configurable height.

- Now rts camera can lock agents by left click or right click when order UI is closed.

### Changed
- Remove restriction that config key cannot conflict with each other.

- Now opening mod menu will cause HUD temporarily enabled to show the menu.

### Fixed
- Fix the problems that when game is paused, the rts camera cannot be rotated by putting mouse on the edge on the screen.

## [1.0.10] - 2020-04-14
### Fixed
- Fix the bug that after clicking "toggle HUD" button in mod menu, the mod menu is not closed.

## [1.0.9] - 2020-04-14
### Added
- Add toggle HUD feature.

  You can press `]` key to toggle HUD. Or if you rebind the key and forget what you have set it to, you can press `Home` key to toggle UI, which always works.

### Changed
- Change default key for disable death to `End`.

### Fixed
- Fix the problem that targeting reticule is shown when rts camera is enabled and player is using ranged weapon.

- Fix a crash when switching to free camera after victory.

## [1.0.8] - 2020-04-13
### Fixed
- Fixed the bug that display message option is not saved.

## [1.0.7] - 2020-04-13
### Changed
- Change slow motion mode logic.

### Add
- Add hot key for slow motion mode. Default to ' key.

- Add display message option.

## [1.0.6] - 2020-04-12
### Added
- Save config for "change combat ai" and "use realistic blocking" options.

## [1.0.5] - 2020-04-12
### Fixed
- Support Bannerlord e1.1.0

### Changed
- Move "use realistic blocking" and "change combat ai" feature to another mod called "EnhancedMission Change AI".

## [1.0.4] - 2020-04-12
### Added
- Add "use free camera by default" option.

- Use mouse to move camera.

### Changed
- Now RTS camera will not be interrupted by player's death.

### Fixed
- Player can drag on the ground when game is paused or slow down, defictive though.

## [1.0.3-hotfix] - 2020-04-11
- Fix bug that hot key config will be reset to default after each game start.

## [1.0.3] - 2020-04-11
### Fixed
- Fix crash when new config for hot key is created.

### Changed
- Enable smooth mode for rts camera.

## [e1.0.2] - 2020-04-10
### Fixed
- Fix the bug that player can control enemy troop after player dead.

## [e1.0.1] - 2020-04-10
### Added
- Add key rebinding feature.

## [e1.0.0] - 2020-04-09
### Added
- Switch to rts-style camera and issue orders.

  Add player to a individual formation.

- Control your troop after dead.

- Pause mission.

- Adjust mission speed.

- Adjust combat AI.

- Option to use realistic blocking introduced in b0.8.1.

- disable and enable damage.

