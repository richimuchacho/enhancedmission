# Enhanced Mission RTS Camera

这是一个骑马与砍杀2：霸主的mod，提供了包括RTS视角指挥在内的一些功能。

## 功能

- 可切换到rts风格的视角并指挥。

- 玩家死后可以控制其小兵。该小兵将会成为当前队伍的领队。

- 调整战斗AI和可选择更真实的格挡功能放在了另一个mod中：["Enhanced Mission Change AI"](https://www.nexusmods.com/mountandblade2bannerlord/mods/449/).

  因为这些功能可能导致游戏更新后mod发生崩溃，降低mod的兼容性。

- 暂停游戏或调整关卡中的时间流逝速度。

- 自定义热键。

- 保存配置：保存配置的文件夹为`(user directory)\Documents\Mount and Blade II Bannerlord\Configs\EnhancedMission\`

  主要配置保存在文件`EnhancedMissionConfig.xml`中。

  按键配置保存在文件`GameKeyConfig.xml`中。

  你可以修改配置，但如果你编辑有误或配置文件被移除，配置会被初始化为默认内容。

- 不死模式：开启后任何单位都不会掉血和死亡。

## 如何安装
1. 复制`Modules`文件夹到砍二的安装目录下（例如`C:\Program Files\Steam\steamapps\common\Mount & Blade II Bannerlord - Beta`)，和砍二本体的Modules文件夹合并。

## 如何使用
- 启动游戏启动器，并选择单人模式(Singleplayer)。在Mods选项卡中勾选`EnhancedMission`并点击`Play`。

  之后正常进行游戏。

- 进入关卡（即进入场景）后：

  - 默认情况下按`O(字母)`键来打开本mod的菜单，你可以在其中访问本mod的所有功能。

    或者你可以用以下默认快捷键：

  - 按`F10`键来切换rts风格视角。

  - 按`F`键或`F10`键来在玩家死后控制其小兵。

  - 按`End`键来切换不死模式。

  - 按`[`键来暂停游戏。

  - 按`'`键来切换变速模式。

  - 按`]`键来开关HUD。若你重新绑定了按键，又忘了你把开关HUD的按键设成了什么，你可以用`Home`键来开关HUD。

- 如何在rts风格的相机中进行游戏：

  - 在关卡（场景）中，按`F10`来切换到rts相机。

  - 你的玩家角色会被加入到mod菜单中选定的编队中。

  - 用`W`, `A`, `S`, `D`, `空格`, `Z`和鼠标中键来移动相机。

  - 用`shift`来加速移动相机。

  - 移动鼠标来旋转相机。或者当命令面板打开后，右键拖动鼠标来旋转相机。

  - 选中部队后左键在地面上拖动来改变部队的位置，方向和宽度。

    - 按住`ctrl`再拖动来让选中的多个编队前后排列。

  - 按住`ctrl`再滚动鼠标中键来调整镜头移动速度。

  - 当命令面板关闭后，单机左键或右键来切换镜头锁定的士兵，移动镜头来解除锁定。

## 解决问题
- 若启动器无法启动：

  - 卸载所有第三方mod，然后一个个重装它们来找出哪个mod导致了启动器不能启动。

- 若提示"Unable to initialize Steam API":

  - 请先启动Steam，并确保砍二在你登录的Steam账号的库中.

- 若mod启动后游戏崩溃：

  - 请取消载入该mod并等待mod更新。

    你可以选择告诉我重现崩溃的步骤。

- 若你忘了你设置的用哪个按键打开mod菜单：

  - 你可以移除配置文件，这样按键会恢复为默认。

## 联系我
* 请发邮件到：lizhenhuan1019@qq.com
