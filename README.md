# Enhanced Mission RTS Camera

A mod for Mount&Blade Bannerlord that provides features in mission such as RTS camera.

## Features

- Switch to rts-style camera.

- Control your troops after you die.

- Pause game and adjust time speed.

- Hotkey rebinding.

- Realistic blocking and adjusting combat AI are supported in another mod called ["Enhanced Mission Change AI"](https://www.nexusmods.com/mountandblade2bannerlord/mods/449/).

  Because supporting them may cause the mod crash after beta update and break compatibility.

- Configuration saving. The configuration is saved in directory `(user directory)\Documents\Mount and Blade II Bannerlord\Configs\EnhancedMission\`.
  
  The main config is saved in file `EnhancedMissionConfig.xml`.

  The hot key config is saved in file `GameKeyConfig.xml`.

  You can modify them manually, but if you edit it incorrectly or remove them, the configuration will be reset to default.

- Undead mode. HP of All agents will not change after switched on.

## How to install
1. Copy `Modules` folder into Bannerlord installation folder(For example `C:\Program Files\Steam\steamapps\common\Mount & Blade II Bannerlord - Beta`). It should be merged with `Modules` of the game.

## How to use
- Start the launcher and choose Single player mode. In `Mods` panel select `EnhancedMission (RTS Camera)` mod and click `PLAY`.

  Then play the game as usual.

- After entering a mission (scene):

  - Press `O(letter)` (by default) to open menu of this mod. You can access the features of this mod in it.

    Or you can use the following hotkeys by default:

  - Press `F10` to switch between rts-style camera and main agent camera.

  - Press `F` key or `F10` key to control one of your troops after you being killed.

  - Press `End` to disable death.

  - Press `[` key to pause game.

  - Press `'` key to toggle slow motion.

  - Press `]` key to toggle HUD. Or if you rebind the key and forget what you have set, you can also press `Home` key to toggle UI, which always works.

- How to play in rts camera:

  - In a mission, press `F10` to switch to rts camera.

  - Your player character will be added to the formation chosen in mod menu.

  - Use `W`, `A`, `S`, `D`, `Space`, `Z` and mouse middle button to move the camera.

  - Use `shift` to speed up camera movement.

  - Move your mouse to rotate the camera, or when order panel is opened, drag right button to rotate the camera.

  - Left click on the ground and drag to change the position, direction and width of the formation you selected.

    - Hold `ctrl` when drag to arrange multiple formations vertically.

  - Hold `ctrl` and scroll mouse to adjust camera movement speed.

  - When order panel is closed, you can left click or right click to lock the camera to soldiers. Move the camera to unlock it.

## Troubleshoot
- If the launcher can not start:

  - Uninstall all the third-party mods and reinstall them one by one to detect which one cause the launcher cannot start.

- If it shows "Unable to initialize Steam API":

  - Please start steam first, and make sure that Bannerlord is in your steam account.

- If the game crashed after starting:

  - Please uncheck the mod in launcher and wait for mod update.

    Optionally you can tell me the step to reproduce the crash.

- If you forget the hotkey you set for opening menu:

  - you can remove the config file so that it will be reset to default.

## Contact with me
* Please mail to: lizhenhuan1019@qq.com
